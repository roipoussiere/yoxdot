from time import sleep
from sys import argv, exit as sys_exit
import os.path as op
import yaml

from FoxDot import Players
from FoxDot import SCLang

WATCH_PERIOD = 0.1
PLAYERS_PREFIX = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
PLAYERS_ATTRS = [ 'dur', 'oct', 'amp' ]
PLAYERS_DEF = {
    'Pads': SCLang._SynthDefs.pads,
    'Bass': SCLang._SynthDefs.bass,
    'Blip': SCLang._SynthDefs.blip,
}

if len(argv) == 1:
    print('Usage: %s music.yml' % argv[0])
    sys_exit(1)

target_path = op.abspath(argv[1])
all_players = {}
last_ts = 0

print('Watching changes for %s...' % target_path)

while True:
    ts = int(op.getmtime(target_path) * 10**6)

    if ts != last_ts:
        last_ts = ts

        with open(target_path) as stream:
            try:
                data = yaml.safe_load(stream)
            except yaml.YAMLError as exc:
                print(exc)
        
        # print('input data', data)

        for group_name, group_players in data.items():
            print('--- group %s ---' % group_name)

            player_id = 1
            player_name = ''
            player_beats = ''
            player_attrs = {}

            for players in group_players:
                for p_type, p_data in players.items():
                    if p_type[0] in PLAYERS_PREFIX:
                        player_name = group_name + str(player_id)
                        print(player_name, p_type)
                        player_beats = p_data
                        all_players[player_name] = Players.Player()
                        PLAYERS_DEF[p_type]

                for p_attr_name, p_attr_val in players.items():
                    if p_attr_name[0] not in PLAYERS_PREFIX:
                        if p_attr_name in PLAYERS_ATTRS:
                            player_attrs[p_attr_name] = p_attr_val
                            print('- ', p_attr_name, p_attr_val)

                all_players[player_name] >> SCLang._SynthDefs.pads(player_beats, **player_attrs)
                player_id += 1

            print()

    try:
        sleep(WATCH_PERIOD)
    except KeyboardInterrupt:
        print('\nExiting Yoxdot')
        sys_exit(1)
