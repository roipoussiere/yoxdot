# YoxDot

Yaml based live coding running with FoxDot.

Work in progress.

## Motivation

The FoxDot project is awesome, but I want to try to use it with a new syntax based in Yaml.

## Installation

python -m venv .venv
pip install -r requirements

## Usage

python yoxdot.py test.yml

## File syntax

Music files are defined as Yaml files containing a list of dictionnaries, where each item is a player. The player type is defined as a key, among its parameters.

Example:

```yaml
- pads: [0, 1, 2, 3, 0]
  dura: [1, 2]
- play: x-o-
  ampl: 3
```

The yaml file above is equivalent to the following FoxDot code:

```py
p1 >> pads([0, 1, 2, 3], dur=[1, 2])
d1 >> play("x-o-", amp=3)
```

See also:
- `test.yml` for an overview current features;
- `sample.yml` for an overview of planned features.
